
/********************************************************************************
 * File:        convolutionner_driver.c
 * Author:      Loïc Brasey
 * Email:       loic.brasey@heig-vd.ch
 * Date:        2024-06-10
 * Version:     1.0
 *
 * Description: Kernel driver for the de1-soc convulutionner fpga IP 
 *
 * License:     MIT
 *
 *
 * Notes:       This is a part of the the project: https://gitlab.com/Brasey-on-github/scf-convolution
 *
 ********************************************************************************/

#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h> /* Needed for file_operations */
#include <linux/slab.h> /* Needed for kmalloc */
#include <linux/uaccess.h> /* copy_(to|from)_user */
#include <linux/string.h>
#include <linux/string.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/of.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>

// REG offset
#define CONST_REG 0x00
#define REQ_REG 0x04
#define STATUS_REG 0x08
#define OUTPUT_BUF 0x800
#define INPUT_BUF 0x100
#define KERNEL_BUF 0x10

// Const id
#define CONST_VAL 0xBADB100D

// CDEV Param
#define MAJOR_NUM 97
#define DEVICE_NAME "convolutionner"

// Enregistrement des information du module
MODULE_LICENSE("GPL");
MODULE_AUTHOR("LOIC");
MODULE_DESCRIPTION("fpga driver convolutioner");
MODULE_INFO(intree, "Y");

/********************** Private Data *****************************************/

struct priv {
	// Type de device
	struct device *dev;

	// cdev handle
	struct cdev cdevice;
	struct class *devclass;

	// Infos mémoire physique
	struct resource *mem_info;

	// Mappage virt. <-> phys.
	void *mapped_mem_ptr;

	uint32_t *constant_id_ptr;
	uint32_t *request_conv_ptr;
	uint32_t *status_ptr;

	uint8_t *input_buffer_ptr;
	uint8_t *kernel_buffer_ptr;
	uint8_t *output_buffer_ptr;

	// Accesseur au différent registre I/O

	const char *name;
};

/*********************************** INPUT CDEV *****************************************************/

#define MATRIX_END 255
#define OUTPUT_END 195

// open call-back
static int convolutionner_open(struct inode *inodep, struct file *filep)
{
	struct priv *priv = container_of(inodep->i_cdev, struct priv, cdevice);
	filep->private_data = (void *)priv; // default selection
	return 0;
}

// write call-back
// get from the user the value to process with the actual convolutionnerd value
static ssize_t convolutionner_write(struct file *filp, const char __user *buf,
			    size_t count, loff_t *ppos)
{
	struct priv *priv = (struct priv *)filp->private_data;
	int _;
	if ((count == 0) || (count + *ppos > MATRIX_END + 1)) {
		return 0;
	}

	_ = copy_from_user(priv->input_buffer_ptr + *ppos, buf, count);
	if(_ != 0){
		dev_err(priv->dev, "\n write error\n");
		return 0;
	}
	*ppos += count;

	return count;
}

// read call-back
// get from the user the value to process with the actual convolutionnerd value
static ssize_t convolutionner_read(struct file *filp, char __user *buf,
			    size_t count, loff_t *ppos)
{
	struct priv *priv = (struct priv *)filp->private_data;
	int _;
	if ((count == 0) || (count + *ppos > OUTPUT_END + 1)) {
		return 0;
	}

	_ = copy_to_user(buf, priv->output_buffer_ptr + *ppos, count);
	if(_ != 0){
		dev_err(priv->dev, "\n read error\n");
		return 0;
	}
	
	*ppos += count;

	return count;
}

static loff_t convolutionner_llseek(struct file *file, loff_t offset, int whence)
{
    loff_t new_pos = 0;

    switch (whence) {
    case SEEK_SET:
        new_pos = offset;
        break;
    case SEEK_CUR:
        new_pos = file->f_pos + offset;
        break;
    case SEEK_END:

        new_pos = MATRIX_END;
	break;
    default:
        return -EINVAL;
    }

    if (new_pos < 0 || new_pos > MATRIX_END)
        return -EINVAL;

    file->f_pos = new_pos;
    return new_pos;
}

#define IOCTL_START 100
#define IOCTL_KERNEL 101
#define IOCTL_STATUS 102




static long convolutionner_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct priv *priv =(struct priv *)filp->private_data;
	int _;
	switch(cmd){
		case IOCTL_START:
			iowrite8(1,priv->request_conv_ptr);
			break;
		case IOCTL_KERNEL:
			_ = copy_from_user(priv->kernel_buffer_ptr, (void*) arg, 9);
			if(_ != 0){
				dev_err(priv->dev, "\n IOCTL error\n");
				return 0;
			}
			break;
		case IOCTL_STATUS:
			return ioread32(priv->status_ptr);

		default:
			return -1;
			
	}
	return 0;
}

const static struct file_operations convolutionner_fops = {
	.owner = THIS_MODULE,
	.read = convolutionner_read,
	.write = convolutionner_write,
	.unlocked_ioctl = convolutionner_ioctl,
	.open = convolutionner_open,
	.llseek = convolutionner_llseek,
};

// fonction pour changer les permissions du device node
static char *devnode( struct device *dev, uint16_t *mode)
{
	if (!mode)
		return NULL;
	if (dev->devt == MKDEV(MAJOR_NUM, 0))
		*mode = 0666;
	return NULL;
}


/*********************************** Platform Driver Setup ******************************************/

// fonction d'initalisation du driver
static int _probe(struct platform_device *pdev)
{
	struct priv *priv;
	int rc;
	dev_t devNo;

	dev_info(&pdev->dev, "io_probe() called !\n"); //Logging

	// Alloc private struct
	priv = kmalloc(sizeof(*priv), GFP_KERNEL);
	if (priv == NULL) {
		dev_err(&pdev->dev,
			"Failed to allocate memory for private data !\n");
		rc = -ENOMEM;
		goto kmalloc_fail;
	}

	// Bind private data
	platform_set_drvdata(pdev, priv);

	// Init. private data
	priv->dev = &pdev->dev;
	priv->name = DEVICE_NAME;

	// Getting memory info
	priv->mem_info = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (priv->mem_info == NULL) {
		dev_err(&pdev->dev,
			"Failed to get memory resource from device tree !\n");
		rc = -EINVAL;
		goto get_mem_fail;
	}

	// Remapping phys. memory
	dev_info(&pdev->dev,"start : %u, size: %u\n",priv->mem_info->start,resource_size(priv->mem_info));

	priv->mapped_mem_ptr =
		ioremap(priv->mem_info->start, resource_size(priv->mem_info));
	if (priv->mapped_mem_ptr == NULL) {
		dev_err(&pdev->dev, "Failed to map memory !\n");
		rc = -EINVAL;
		goto ioremap_fail;	
	}

	// set-up PTR(s)
	priv->constant_id_ptr = (uint32_t *)(priv->mapped_mem_ptr + CONST_REG);
	priv->request_conv_ptr = (uint32_t *)(priv->mapped_mem_ptr + REQ_REG);
	priv->status_ptr = (uint32_t *)(priv->mapped_mem_ptr + STATUS_REG);
	priv->input_buffer_ptr = (uint8_t *)(priv->mapped_mem_ptr + INPUT_BUF);
	priv->kernel_buffer_ptr =
		(uint8_t *)(priv->mapped_mem_ptr + KERNEL_BUF);
	priv->output_buffer_ptr =
		(uint8_t *)(priv->mapped_mem_ptr + OUTPUT_BUF);

	// Log ID
	dev_info(&pdev->dev, "Const ID : %x\n", ioread32(priv->constant_id_ptr));


	// Enregistrement du char device

	devNo = MKDEV(MAJOR_NUM, 0);
	rc = register_chrdev_region(devNo, 1, "convolutionner_region");
	if (rc) {
		dev_err(&pdev->dev, "Register chardev region error\n");
		rc = -EINVAL;
		goto cdev_fail;
	}

	// Creation d'un class pour le device node
	priv->devclass = class_create(THIS_MODULE,"convolutionner_class");
	if (IS_ERR(priv->devclass)) {
		dev_err(&pdev->dev, "\ncan't create class");
		rc = -1;
		goto class_fail;
	}

	// Changement des permissions
	priv->devclass->devnode = devnode;

	if (IS_ERR(device_create(priv->devclass, NULL, devNo, NULL,
				 DEVICE_NAME))) {
		dev_err(&pdev->dev, "can't create device /dev/???");
		rc = -1;
		goto dev_fail;
	}

	// Instanciation du char device
	cdev_init(&priv->cdevice, &convolutionner_fops);
	rc = cdev_add(&priv->cdevice, devNo, 1);
	if (rc) {
		dev_err(&pdev->dev, "cdev add error\n");
		rc = -EINVAL;
		goto cdev_add_fail;
	}



	dev_info(&pdev->dev, "Driver ready!\n"); // logging
	return 0;

// Error clean-up

cdev_add_fail:
	cdev_del(&priv->cdevice);
	device_destroy(priv->devclass, devNo);
dev_fail:
	class_destroy(priv->devclass);
class_fail:
	unregister_chrdev_region(MKDEV(MAJOR_NUM, 0), 1);
cdev_fail:
	iounmap(priv->mapped_mem_ptr);
ioremap_fail:
get_mem_fail:
	kfree(priv);
kmalloc_fail:
	return rc;
}

static int _remove(struct platform_device *pdev)
{
	struct priv *priv = platform_get_drvdata(pdev);
	dev_t devNo = MKDEV(MAJOR_NUM, 0);

	// Logging
	dev_info(&pdev->dev, "Removing driver...\n");

	cdev_del(&priv->cdevice);
	device_destroy(priv->devclass, devNo);

	class_destroy(priv->devclass);

	unregister_chrdev_region(devNo, 1);
	iounmap(priv->mapped_mem_ptr);
	kfree(priv);

	return 0;
}

static const struct of_device_id io_driver_id[] = {
	{ .compatible = "de1_fpga_convolutionner" },
	{ /* END */ },
};

static struct platform_driver io_driver = {
	.driver = {
		.name = "convolutionner-driver",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(io_driver_id),
	},
	.probe = _probe,
	.remove = _remove,
};

module_platform_driver(io_driver);