#!/bin/bash

for i in {0..255}
do
 ./devmem2 $((0xff200100 + $i)) b $i
done

for i in {0..8}
do
 if [ $i -eq 4 ]; then
        e=1
 else
	e=0
 fi
 ./devmem2 $((0xff200010 + $i)) b $e 
done
