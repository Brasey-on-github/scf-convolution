/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * Author               : Loic Brasey
 * Date                 : 05.03.24
 *
 *****************************************************************************************
 * Brief: test for convolutionner driver
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 1.0    05.03.24    LBY          Initial
 *
*****************************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>

int main()
{
        int fd = -1;
        printf("Driver testing \n");

        // open cdev
        fd = open("/dev/convolutionner",O_SYNC | O_RDWR);
        if(fd   == -1){
            printf("cant open /dev/convolutionner\n");
            return 0;
        }

        char buff[256];
        for (int i = 0; i < 256 ; i++){
                buff[i] = i;
        }

        write(fd,buff,256);
        lseek(fd,0,SEEK_SET);

        char kernel[9] = {0,0,0,
                          0,1,0,
                          0,0,0};
        ioctl(fd,101,kernel);
        ioctl(fd,100,NULL);
        sleep(1);
        read(fd,buff,195);
        for (int i = 0; i < 195 ; i++){
                printf("%02hhx\n",buff[i]);
        }



    return 0;
}