/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * Author               : Loic Brasey
 * Date                 : 05.03.24
 *
 *****************************************************************************************
 * Brief: SHarpen an image : https://setosa.io/ev/image-kernels/
 *  I used easyppm from : https://github.com/fmenozzi/easyppm
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 1.0    05.03.24    LBY          Initial
 *
*****************************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Function to load a binary PGM image into a 2D array
unsigned char* loadPGM(const char* filename, int* width, int* height) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        fprintf(stderr, "Cannot open file %s\n", filename);
        return NULL;
    }

    // Read the header
    char magicNumber[3];
    fscanf(file, "%2s", magicNumber);
    if (strcmp(magicNumber, "P5") != 0) {
        fprintf(stderr, "Unsupported PGM format (must be P5)\n");
        fclose(file);
        return NULL;
    }

    // Skip comments
    int ch;
    while ((ch = fgetc(file)) == '#') {
        while (fgetc(file) != '\n');
    }
    ungetc(ch, file);

    // Read image dimensions and max value
    int maxVal;
    fscanf(file, "%d %d %d", width, height, &maxVal);
    if (maxVal != 255) {
        fprintf(stderr, "Unsupported max value (must be 255)\n");
        fclose(file);
        return NULL;
    }

    // Allocate memory for the image
    unsigned char* image = (unsigned char*)malloc(*height * *width * sizeof(unsigned char));


    // Read pixel data
    fgetc(file);  // Read the newline character after maxVal
    fread(image, sizeof(unsigned char), *width * *height, file);

    fclose(file);
    return image;
}

// Function to write the PGM image data to a file and free the allocated memory
void unloadPGM(const char* filename, unsigned char* image, int width, int height) {
    FILE* file = fopen(filename, "wb");
    if (file == NULL) {
        fprintf(stderr, "Cannot open file %s for writing\n", filename);
        return;
    }

    // Write the header
    fprintf(file, "P5\n%d %d\n255\n", width, height);

    // Write pixel data
 
    fwrite(image, sizeof(unsigned char), width * height, file);

    fclose(file);

    free(image);
}



int main(int argc, char *argv[])
{
        
        if (argc != 2){
                printf("exepected 1 arg : path to the ppm image\n");
                return -1;
        }


        // open cdev
        int fd = open("/dev/convolutionner",O_SYNC | O_RDWR);
        if(fd   == -1){
            printf("cant open /dev/convolutionner\n");
            return 0;
        }

        // load kernel
        signed char kernel[9] = {0,-1,0,
                          -1,5,-1,
                          0,-1,0};
        ioctl(fd,101,kernel);

        // load image

        int height,width;
        unsigned char* src_img = loadPGM(argv[1],&width,&height);
        printf("image loaded\n");
        unsigned char*dest_img = calloc(height * width,1);

        // convolute
         printf("start conv\n");
        int block_i, block_j, i, j;
        for(block_i = 0; block_i < (height -2) / 14; block_i++){
                for(block_j = 0; block_j < (width -2) / 14; block_j++){
                        for(i = 0; i < 16; i++){
                                write(fd,src_img+ (block_i * 14 + i) * width + block_j * 14 ,16);
                        }
                        ioctl(fd,100,NULL);
                        lseek(fd,0,SEEK_SET);
                        while(ioctl(fd,102,NULL));
                        for(i = 0; i < 16; i++){
                                read(fd,dest_img + (block_i * 14 + i + 1) * width + block_j * 14 + 1,14);
                        }
                        lseek(fd,0,SEEK_SET);
                }
        }

        unloadPGM("output.pgm",dest_img,width,height);
        free(src_img);

        printf("Result image: output.pgm");

        close(fd);

    return 0;
}
