# SCF-Convolution
Loïc Brasey

# Introduction 
Ce travail a pour but de réaliser un système complet pour permettre de
déléguer le calcul d’une convolution sur une image au FPGA.

# Spécifications

Voir [la donné](./Documentation/SOCF_Lab09-convolution_2024.pdf) du labo.

# Première approche
Pour réaliser un telle système,la première étape sera de de faire un diagramme de séquence simple pour se donner un ordre d'idée des chose à faire pour réaliser cette tâche :

[![schéma en ligne](https://mermaid.ink/img/pako:eNplUk1rAyEQ_SuDpxYSaPN18FAIhJYeSkLboxBkd3Yjibodx5QS8t_rrpGE1rn45j2fM6MnUfkahRSBNePK6Ja0HR8nykFaQxKUeF0rATpAjaH6w7guslQp2XV2a6xusQd7JIeHtOvBOnIROfzeZmF769jH4PogYXCEZfUVTTBsfKEeJaw0a9gQduQrDMG49sJNJHySdqFBYmAPz5uX5YWaygHBzZnE3FXeHf0h9v73F-VMwjsyGTwiNOTtrcu8XO4D_7t9MRyMVDq9dgPj8VOqvHSQYZntJMNpKTTDWakmw3kpIMNFnlUfYiQsktWmTq936mVK8A4tKiHTtta0V0K5c9LpyP7jx1VCMkUcidjV18cWstGHkLJYG_b0lr_D8CvOv-LnrHc?type=png)](https://mermaid.live/edit#pako:eNplUk1rAyEQ_SuDpxYSaPN18FAIhJYeSkLboxBkd3Yjibodx5QS8t_rrpGE1rn45j2fM6MnUfkahRSBNePK6Ja0HR8nykFaQxKUeF0rATpAjaH6w7guslQp2XV2a6xusQd7JIeHtOvBOnIROfzeZmF769jH4PogYXCEZfUVTTBsfKEeJaw0a9gQduQrDMG49sJNJHySdqFBYmAPz5uX5YWaygHBzZnE3FXeHf0h9v73F-VMwjsyGTwiNOTtrcu8XO4D_7t9MRyMVDq9dgPj8VOqvHSQYZntJMNpKTTDWakmw3kpIMNFnlUfYiQsktWmTq936mVK8A4tKiHTtta0V0K5c9LpyP7jx1VCMkUcidjV18cWstGHkLJYG_b0lr_D8CvOv-LnrHc)

Dans cette version, je prends une image `.pgm` car c'est un format sans compression qui sera simple à traiter avec un plan de couleur.

Dans ce projet, je vois trois entité : 

- Un programme userspace : Il aura pour but d'effectuer le preprocessing et le postprocessing des entrées sorties
- Un driver : Il fera le pont entre la fpga et le programme userspace en transférant les données de l'un à l'autre.
- Un design fpga : Il aura pour but de réaliser la convolution. 

Pour ce qui est des interfaces de chaque composant, je fais le choix de ne rien définir à l'avance
et de commencé à développer les trois entités de la plus contrainte (fpga) à la moins contrainte (userspace app).
Cela permettra d'être plus flexible dans le développement du système.

La seul chose qui est pour l'instant assumé c'est que l'userspace app prends une image au format ppm (rgb ou grayscale) et un kernel 3x3 et retourne une image ppm convolué en sortie.
J'envisage tous de même d'ajouté plus de paramètre d'entrée.  

## FPGA

Pour commencé, je me renseigne sur ce qu'il est possible de faire en terme de mémoire dans la fpga.
Après avoir analysé les différent IP déjà présentent dans Quartus je n'ai rien trouvé de satisfaisant.

Je suis donc partie sur un désigne "homemade" à partir de certain de mes travaux précédent.

L'IP aura une interface slave AXI pour s'interfacer avec le HPS.


Je choisi de partir sur un module qui fera une convolution d'un bloc de 16x16 bytes.
Le kernel de convolution sera 3x3.
À la sortie, on aura donc une matrice 14x14.

L'interface aura ce plan d'addressage :

|Offset (idx)     | Data                    | RW |       
|-----------------|-------------------------|----|       
|0x00	    (0)   | Constant (0xBADB100D)   | R  | 
|0x04	    (1)   | Start process           | W  |        
|0x08	    (2)   | Status        	    | R  | 
|0x10	  (4-6)   | Kernel input            | W  | 
|0x100   (64-...) | Data input              | W  | 
|0x800  (512-...) | Data output             | R  | 
